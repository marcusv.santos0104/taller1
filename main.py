from ventas import *

if __name__ == '__main__':
    print("Programa Principal")

invent = ""
caja = ""
menu = 0
while menu != 9:
    print("1. Abrir Caja.")
    print("9. Cerrar Caja.")
    menu = int(input())
    if menu == 1:
        print("1. Agregar inventario.")
        print("2. Buscar inventario.")
        print("3. Agregar caja.")
        print("4. Buscar caja.")
        print("9. Cerrar caja.")
        menu = int(input())
        if menu == 1:
            invent = Inventario()
            invent.crearItem()
        elif menu == 2:
            invent.buscarItem()
        elif menu == 3:
            caja = Cajas()
            caja.crearCaja()
        elif menu == 4:
            caja.buscarCaja()
