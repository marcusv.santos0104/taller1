import random

class Cajas:
    id = ""
    nombre = ""

    def crearCaja(self):
        print("____Crear Caja____")
        self.id = random.randint(1, 10)
        self.nombre = input("Nombre: ")
        print("Creado con exito.")

    def buscarCaja(self):
        print("ID:", self.id, "| Nombre:", self.nombre)


class Inventario:
    lugar = ""
    nombreItem = ""

    def crearItem(self):
        print("____Crear Inventario____")
        self.lugar = input("Lugar: ")
        self.nombreItem = input("Nombre: ")
        print("Creado con exito.")

    def buscarItem(self):
        print("Lugar:", self.lugar, "| Nombre:", self.nombreItem)
